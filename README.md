## Tide Linux
### A new independent linux distribution with its own custom package manager
![Tide Logo](logo.png)
#### Information
Tide Linux is a passion project meant to explore the creation of linux distributions
It is not meant to replace any of the well known distributions nor does it aim to bring any groundbreaking changes to the linux desktop

#### Package Manager
Tide Linux uses the [Bubble Package Manager](https://gitlab.com/bubble-package-manager/bpm) (BPM), a simple package manager programmed in GO. It offers simple to use commands like `bpm install` and `bpm remove` and an integrated source package builder so that it can install both binary and source packages 

#### Download
At this moment in time no fully installable ISO exists, there are test images avaiable:
- [Tide Linux 0.2-Alpha](https://drive.google.com/file/d/1ooNWESDewf4u938_Fq_-vrf46B_U4PdH/view?usp=sharing) (Not installable, cannot compile packages)
- [Tide Linux 0.3-Alpha-minimal](https://drive.google.com/file/d/1PXMWn1LjJQlPSkZd6P9wBA1xdbuto8J2/view?usp=sharing) (Not installable, cannot compile packages)
- [Tide Linux 0.3-Alpha-full](https://drive.google.com/file/d/1Lydw1V2y2xRa6PiJQKBX-S0JStRqXAIZ/view?usp=sharing) (Semi-installable, can compile packages, but limited in space)
- [Tide Linux 0.4-Alpha-systemd](https://drive.google.com/file/d/1Qb7KHwNJ18UwarkAFVw8ZRZAeVqhAD32/view?usp=sharing) (Fully installable)

#### Installation Guide
Make sure you are using Tide-Linux 0.4-Alpha or above as earlier versions cannot be installed. You can find the guide [here](https://gitlab.com/tidelinux/gitlab-profile/-/blob/main/Guides/installation-guide.md)

### Contributors
- [EnumDev](https://gitlab.com/EnumDev) (Lead Maintainer)
- [FrenchGuy](https://gitlab.com/FrenchGuy) (Logo Creator)

#### Contributing
Anyone is able to contribute to Tide Linux's development by submitting packages or offering to maintain an existing package
