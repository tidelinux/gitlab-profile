# Tide Linux Mirror Setup Guide
## Preparation
- Make sure you have an internet access  
- Make sure you have downloaded the binary repository tarball from [here](https://drive.google.com/file/d/1velwSE19tcRIGacn668itJo-wR4X5QON/view?usp=sharing)
- Make sure the following programs are present in your system:  
  - Python
  - Tar
  - Gzip
## Setting up the mirror
- Extract the tarball using the following command
```bash
tar -xvf TideLinux-0.4-Alpha-binary-repo.tar.gz
```
- Enter the newly extracted 'binary' directory
- You can start an http file server on port 8000 using the following command
```
python -m http.server
```
- To get your IP address either run `ifconfig` or `ip addr`. You will be able to access your repository from `http://<IPv4 address>:8000`