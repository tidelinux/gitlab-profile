# Tide Linux Installation Guide
## Before continuing with the installation please read the following
- Tide Linux is nowhere near a finished state. Expect stuff to break
- Due to Tide being in such an unfinished state there is a possibility you may be unable to directly update from one Alpha Tide Linux version to another. You will likely have to do a full reinstallation
- Tide Linux is a pet-project. You should not expect to use this as your primary distro or daily driver
- At its current state Tide Linux does not offer a lot of drivers for hardware like Nvidia GPUs, Network Cards, etc. You may need to compile those drivers yourself. There is also no Wi-Fi support at this moment in time
- Tide Linux is supposed to follow a stable-release model with a new Tide version coming out every 6 months, but while in Alpha package updates may be much more infrequent and come out at random
- At this point in time there is no official repository mirror. You will have to download the binary repository tarball [here](https://drive.google.com/file/d/1velwSE19tcRIGacn668itJo-wR4X5QON/view?usp=sharing) and setup a file server to access the repository. You can find a guide for this [here](https://gitlab.com/tidelinux/gitlab-profile/-/blob/main/Guides/mirror-setup-guide.md)
- Tide packages are at a very unfinished state. Dependencies are not well defined and you may have to manually download some packages that you would expect to be installed as dependencies

## Preparation
Before installing Tide Linux you need to setup a Live USB to boot the installation media from. You can find the latest Tide Linux ISO [here](https://drive.google.com/file/d/1Qb7KHwNJ18UwarkAFVw8ZRZAeVqhAD32/view?usp=sharing)
There are multiple ways in which you can boot from an ISO file
- If using a Virtual Machine you should be able to directly mount the ISO file from your VM settings
- If you wish to boot on real hardware you will have to flash the ISO onto a USB Flash Drive using tools like
  - [Rufus](https://rufus.ie/en/) (GUI) on Windows systems (If the flash drive fails to boot try flashing using the DD image mode)
  - [Balena Etcher](https://etcher.balena.io/) (GUI) on any system
  - [dd]() (CLI) on any Unix system by typing the following command `# dd if=<ISO file> of=<Output device> bs=1M status=progress`
  - [Ventoy](https://github.com/ventoy/Ventoy) to create a multi-boot USB flash drive
## Logging in
After booting into the ISO you will be greeted with a login screen. You may login as either the `root` user for or the `tide` user with the password `tidelinux`. If you decide to login as the `tide` user you will have to use either `sudo` or `doas` to run commands with superuser privileges. Commands that need superuser privileges are prefixed with '#' while others are prefixed with '$'
## Understanding partitioning on Linux
**Note: this guide will only go over how to create a single-boot system and won't explain how to partition the system for dual-booting**
First of all you need to decide on a partition scheme for your system
Systems generally boot in two different ways. UEFI systems boot through an EFI file in an EFI partition. BIOS systems read and boot from the first 512 bytes.
Almost all modern systems should be using UEFI, while older machines may use a BIOS. There is a misconception that UEFI and BIOS are the same thing but this is not true and mixing the two up may end you up with an unbootable system.
You can use the following command to see whether you are on a UEFI or BIOS system
```bash
$ cat /sys/firmware/efi/fw_platform_size
```
If this command returns '64' or '32' then you are using a UEFI system. If the file does not exist then you are using a BIOS system.

UEFI systems are generally paired with the GPT partition table
BIOS systems are generally paired with the MBR partition table

**UEFI system with GPT partition table example layout**
|Partition Number|Partition Type|Mount Point|Recommended Size|Notes
|--|--|--|--|--|
|1|EFI|/mnt/boot/efi|256M|The EFI partition the system will boot from
|2|Linux Filesystem|/mnt|Remaining space (Or up to 64G if using a home partition)| The root partition which contains programs and system-wide configuration files along with other essential files for your system
|3 (Optional)|Linux Filesystem|/mnt/home|Remaining space| The home partition contains user data. It is optional but recommended on large disks as it provides an easy way to reinstall or switch distributions without losing user data

**BIOS system with MBR partition table example layout**
|Partition Number|Partition Type|Mount Point|Recommended Size|Notes
|--|--|--|--|--|
|1|Linux Filesystem|/mnt|Remaining space (Or up to 64G if using a home partition)| The root partition which contains programs and system-wide configuration files along with other essential files for your system
|2 (Optional)|Linux Filesystem|/mnt/home|Remaining space| The home partition contains user data. It is optional but recommended on large disks as it provides an easy way to reinstall or switch distributions without losing user data

After deciding on a partition layout you now need to find the device you want to install Tide Linux to. The following command can help you identify your drives
```bash
$ lsblk -o NAME,SIZE,MODEL
```
This command will output each storage device connected your system along with its partitions under it. SATA drives are listed as `sdX` while NVME drives are listed as `nvme0nX` with X being the letter/number assigned to the **device**. Partitions are listed as `sdXY` or `nvme0nXpY` with Y being the number assigned to each **partition**.
On Linux systems the raw data of storage devices partitions are listed as files under the `/dev` directory. For example a **device** `sda` will be listed under `/dev/sda`. A **partition** `sda1` will instead be listed under `/dev/sda1`. When partitioning we want to write to the raw data of the device instead so we use `/dev/<your device>` and not the mountpoint to which we will mount the filesystem of each partition later on
Make sure you can differentiate your **device** from your **partitions** before continuing
## Partitioning
**Warning: Continuing from this point on will erase all data on the disk you are installing to**
**Note: This guide will use the EXT4 filesystem which is the most stable and not alternative filesystems like BTRFS**
Now that you have identified the **device** you want to install Tide Linux to it's time to begin partitioning your drive by erasing any current partition tables present on it. You can do this through the following command. OnTh a new unused device you can skip this command
```
# wipefs -a /dev/<your device>
```
Beginners are strongly advised to use the `cfdisk` utility to partition their device as it provides an easy to use terminal user interface (TUI). Experienced Linux users may use the `fdisk` utility. To use either partitioning program you may use one of the following command
```
# cfdisk /dev/<your device>
```
```
# fdisk /dev/<your device>
```
Create the partition scheme you have decided to use using either program and then write the changes to disk
You can run the `lsblk` command with no arguments to quickly list your devices and partitions to ensure the changes you made were correct

Now it is time to format your **partitions** with the appropriate filesystem for each
If you have created an EFI partition you should format it as `FAT32` with the following command
```
# mkfs.fat -F 32 /dev/<your EFI partition>
```
You can format your root and optional home partition as `EXT4` with the following command
```
# mkfs.ext4 /dev/<your root partition>
# mkfs.ext4 /dev/<your home partition>
```
It is now time to mount your partitions. You may use the mountpoints listed above in the example layouts.
To mount your root partition type the following command
```
# mount /dev/<your root partition> /mnt
```
If created, mount your home partition with the following command
```
# mount /dev/<your home partition> /mnt/home --mkdir
```
If created, mount your EFI partition with the following command
```
# mount /dev/<your EFI partition> /mnt/boot/efi --mkdir
```
You should be able to verify everything has been mounted correctly by typing `lsblk`
## Configuring your BPM repository
**Note: this stage expects you to have already followed the repository setup guide beforehand or know another repository you can use**
The installation media comes with the `nano` and `vim` text editors. Beginners are recommended to use nano
Use your favorite text editor to edit the file at `/etc/bpm.conf`. Here is an example with nano
```
# nano /etc/bpm.conf
```
You will be greeted with the following text
```
compilation_env: []
silent_compilation: false
compilation_dir: "/var/tmp/"
binary_output_dir: "/var/lib/bpm/compiled/"
repositories:
  - name: example-repository
    source: https://my-repo.xyz/
    disabled: true
```
You may edit the name of the example-repository to whatever you'd like. Then change the source to the URL your repository is hosted to. If it is on the local network it may look something like `http://192.168.1.7:8000/`. Finally make sure to remove the `disabled: true` line or set it to false

You should now be able to run the following command to sync all databases. If your repository is not listed in the output then make sure you have the config file or repository set up correctly
```
# bpm sync
```
## Installing the base system
You can start the installation of the base system by entering the following command
```
# bpm install -R=/mnt base linux-lts tzdata dracut
```
When this is done run the following command to copy the BPM configuration to the newly installed system
```
# cp /etc/bpm.conf /mnt/etc/bpm.conf
```
You are now able to 'chroot' into your new system. This means that you will enter an environment that will act similar to having actually booted into the system without actually doing that. You can do this by typing
```
# tide-chroot /mnt
```
If all goes well you will now be chrooted signified by the `[Tide-chroot]` prefix in your prompt
You now need to sync your repositories again in the new system so type the same command again
```
# bpm sync
```
## Creating an Initial Ram Filesystem
You may now create an Initial Ram Filesystem (initramfs) image which is used to provide an initial root file system during the Linux boot process. Run the following command. A few warnings about missing commands is normal when running this command
```
# dracut
```
## Installing microcode
Now you may install the appropriate microcode for your system. Currently only the following are provided
- `linux-firmware-amd` for AMD CPUs and GPUs
- `linux-firmware-nvidia` for Nvidia GPUs
- `linux-firmware-intel` for Intel CPUs and GPUs
You can install these packages using this command
```
# bpm install <packages>
```
## Setting up /etc/fstab
For the following steps you will need to install a text editor. Using the `bpm install` command mentioned above install either `nano` or `vim`

The `/etc/fstab` file on Linux tells the system what partitions should be mounted at boot and where. You can quickly get an fstab file by running the following command which will write the data of one file into fstab. If using the `tide` user account you will want to append `sudo` or `doas` after the pipe `|` symbol and not before `cat`
```
# cat /proc/mounts | tee /etc/fstab
```
Now use your preferred text editor to open `/etc/fstab` and remove all lines that do not start with the partitions your created and formatted earlier. In the end it may look something like this
```
/dev/sda1 /boot/efi vfat...
/dev/sda2 /mnt ext4...
```
## Setting up locales
You can think of locales as the system language(s). You want to generate those otherwise text may look weird or not display at all
To find the locales for each country and writing system this [website](https://lh.2xlibre.net/locales/)
In most cases you will want the American English (en_US) as a backup along with the locale for your native language

After finding all the locales you want to use edit the `/etc/locale.gen` file and uncomment (remove the # symbols) from the lines that contain your desired locales. When this is done run the following command to generate your locales
```
# locale-gen
```
You should be able to see your locales listed in the output of the command. If not go back to editing the file and confirm everything is set up correctly

Now you want to set your primary locale. This can be done by writing the following text in the `/etc/locale.conf` file
```
LANG=<your locale>.UTF-8
```
for example for American English this will look something like this
```
LANG=en_US.UTF-8
```
## Setting the time and date
To begin with you need to find the TZ identifier for your timezone. A list of all time zones and their identifiers can be found [here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

you can set your time zone with the following command
```
# ln -sf /usr/share/zoneinfo/<TZ identifier> /etc/localtime
```
Finally you need to synchronize the hardware clock with the system clock and generate the `/var/lib/hwclock/adjtime` file using the `hwclock` command. Do this by running the following
```
# mkdir -p /var/lib/hwclock/
# hwclock --systohc
```
## Setting a root password
Simply run the `passwd` command with no arguments and type your desired root password twice
## Installing DHCPCD for internet access
Start by installing the `dhcpcd` package using `bpm`
Enable the `dhcpcd` systemd service by running this command
```
# systemctl enable dhcpcd.service
```
## Installing the GRUB bootloader
Start by installing the `grub` package along with `efibootmgr` if you are on a UEFI system

If you are using a UEFI system run the following command to install GRUB
```
# grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Tide
```
If you are using a BIOS system run the following command to install GRUB. Remember to enter the **device** you are installing to. Not any of the **partitions**
```
# grub-install /dev/<your device>
```
Finally create a grub config file by entering the following command
```
# grub-mkconfig -o /boot/grub/grub.cfg
```
## Rebooting
Your system is now fully installed. You can type `exit` to exit from the chroot environment and then `reboot` to reboot your system. Make sure you remove your flash drive from the computer to avoid accidentally booting back into it
## Adding users
After rebooting you will soon realize that you can only login as root. You probably want to add additional users. You can easily do this by running the `useradd` command
```
# useradd -m -G wheel -s /bin/bash <username>
```
Now give your user a password with this command
```
# passwd <username>
```
You should now be able to type `exit` to go back into the login screen and enter the credentials of your new user
## Installing a privilege escalator program
Escalator programs like `sudo` or `opendoas` provide a way for normal users to temporarily run commands with superuser permissions. Tide Linux recommends using `opendoas` as it is slightly more lightweight than sudo and is less prone to exploits but `sudo` may give you slightly more compatibility as it is the default for the majority of linux distributions

### Installing and configuring opendoas
- Install `opendoas` using `bpm`
- As root edit the `etc/doas.conf` file and uncomment the first line starting with `permit` to allow the `wheel` group to use doas.
- If you want to enable the `persist` feature which lets you run doas without entering your password for a specific amount of time uncomment the second line starting with `permit`
- You can now login as a normal user that is in the `wheel` group and attempt to run a command prefixed with `doas`. If the command succeeds then opendoas is set up correctly
### Installing and configuring sudo
- Install `sudo` using `bpm`
- As root edit the `etc/sudoers.d/00-allow-wheel` file and uncomment the second and third line
- You can now login as a normal user that is in the `wheel` group and attempt to run a command prefixed with `sudo`. If the command succeeds then sudo is set up correctly
